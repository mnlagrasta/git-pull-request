# Embarrassingly basic makefile. Feel free to edit these vars to your liking
BINPATH = /usr/local/bin
MANPATH = /usr/local/share/man/man1

test:
	# Simple test for dependancies
	perl -e 'use REST::Client; use Data::Dumper; use JSON; use Term::ReadKey; use Getopt::Std;';

install:
	install -g 0 -o 0 -m 0655 bin/git-pull-request $(BINPATH)/
	install -g 0 -o 0 -m 0644 doc/gitpull-request.1 $(MANPATH)
	git config --global gprbitbucket.url bitbucket.org/api/2.0

uninstall:
	rm $(BINPATH)/git-pull-request
	rm $(MANPATH)/gitpull-request.1
	git config --global --remove-section gprbitbucket
